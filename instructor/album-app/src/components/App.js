import React, { useState, useEffect } from "react";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	NavLink,
} from "react-router-dom";
import axios from "axios";

import "./App.css";
import Banner from "./Banner";
import Album from "./Album";
import CreateAlbumForm from "./CreateAlbumForm";

const App = () => {
	const [albums, setAlbums] = useState([]);
	const [fetchAlbum, setFetchAlbum] = useState(false);

	useEffect(() => {
		axios.get("http://localhost:8088/albums").then((res) => {
			//console.log(res);
			setAlbums(res.data);
		});
	}, [fetchAlbum]); // run this at start and whenever fetchAlbum is changed

	return (
		<Router>
			<div className="app">
				<ul className="nav">
					<li className="nav-item">
						<NavLink
							exact
							className="nav-link text-secondary"
							to="/"
							activeClassName="app"
						>
							Home
						</NavLink>
					</li>
					<li className="nav-item">
						<NavLink
							exact
							className="nav-link text-secondary"
							to="/add"
							activeClassName="app"
						>
							Add Album
						</NavLink>
					</li>
				</ul>

				<Banner />
				<Switch>
					<Route exact path="/">
						<div className="container">
							<div className="row">
								{albums.map((album) => (
									<Album
										key={album.title}
										title={album.title}
										artist={album.artist}
										tracks={album.tracks}
									/>
								))}
							</div>
						</div>
					</Route>
					<Route path="/add">
						<CreateAlbumForm
							fetchAlbum={fetchAlbum}
							setFetchAlbum={setFetchAlbum}
						/>
					</Route>
				</Switch>
			</div>
		</Router>
	);
};

export default App;

// axios.get("http://localhost:8088/albums").then((res) => {
// 	console.log(res);
// 	setAlbums(res.data);
// });

// const albums = [
// {
// 	title: "Album 1",
// 	artist: "Artist 1",
// 	tracks: ["Track 1", "Track 2", "Track 3"],
// },
// 	{
// 		title: "Album 2",
// 		artist: "Artist 2",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 3",
// 		artist: "Artist 3",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// 	{
// 		title: "Album 4",
// 		artist: "Artist 4",
// 		tracks: ["Track 1", "Track 2", "Track 3"],
// 	},
// 	{
// 		title: "Album 5",
// 		artist: "Artist 5",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 6",
// 		artist: "Artist 6",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// ];
