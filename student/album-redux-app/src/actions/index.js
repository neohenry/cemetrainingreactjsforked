export const fetchAlbumsSuccess = (albums) => {
	return {
		type: "FETCH_ALBUMS_SUCCESS",
		payload: albums,
	};
};
