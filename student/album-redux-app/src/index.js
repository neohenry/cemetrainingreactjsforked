import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";

import App from "./components/App";

const albumsReducer = (state = { albums: [] }, action) => {
	console.log(`received ${action.type} dispatch in albumsReducer`);

	switch (action.type) {
		case "FETCH_ALBUMS_SUCCESS":
			return { ...state, albums: action.payload };
		default:
			return state;
	}
};

const store = createStore(albumsReducer); // need reducers
console.log("redux store is created", store.getState());

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("root")
);
