import React from "react";
import { connect } from "react-redux";

import Height from "./Height";
import { addCount } from "./actions";

const App = (props) => {
	return (
		<React.Fragment>
			<div>Count Redux App</div>
			<p>
				count is {props.count}{" "}
				<button
					onClick={() => {
						props.addCount(props.count + 1);
					}}
				>
					Add count
				</button>
			</p>
			<p>weight is {props.weight}</p>
			<Height />
		</React.Fragment>
	);
};

const actionCreators = {
	addCount,
};

const mapStateToProps = (state) => {
	return { count: state.count, weight: state.weight };
};

export default connect(mapStateToProps, actionCreators)(App);
