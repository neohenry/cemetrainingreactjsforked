import React from "react";
import { connect } from "react-redux";

const Height = (props) => <p>height is {props.height}</p>;

const mapStateToProps = (state) => {
	return { height: state.height };
};

export default connect(mapStateToProps)(Height);
