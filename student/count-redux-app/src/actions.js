export const addCount = (count) => {
	// return an action
	return {
		type: "ADD_COUNT",
		payload: count,
	};
};
