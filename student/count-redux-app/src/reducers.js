import { combineReducers } from "redux";

const countReducer = (state = 0, action) => {
	// if (state === undefined) {
	// 	state = 0;
	// }
	console.log(`received ${action.type} dispatch in countReducer`);

	if (action.type === "ADD_COUNT") {
		//console.log("received dispatch in countReducer");
		return action.payload;
	}

	return state;
};

const weightReducer = (state = 100, action) => {
	console.log(`received ${action.type} dispatch in weightReducer`);
	return state;
};

const heightReducer = (state = 185, action) => {
	console.log(`received ${action.type} dispatch in heightReducer`);
	return state;
};

export default combineReducers({
	count: countReducer,
	weight: weightReducer,
	height: heightReducer,
});
