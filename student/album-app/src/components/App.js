//import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
//import axios from "axios";

import "./App.css";
import Banner from "./Banner";
import AlbumsListing from "./AlbumsListing";
import CreateAlbumForm from "./CreateAlbumForm";

const App = () => {
	// const [albums, setAlbums] = useState([]);
	// useEffect(() => {
	// 	console.log("app is mounted");
	// 	axios.get("http://localhost:8081/albums").then((res) => {
	// 		//console.log(res);
	// 		setAlbums(res.data);
	// 	});
	// 	return () => {
	// 		console.log("app is unmounted");
	// 	};
	// }, []);

	return (
		<Router>
			<div className="app">
				<ul>
					<li>
						<Link to="/">Home</Link>
					</li>
					<li>
						<Link to="/add">Add Album</Link>
					</li>
				</ul>
				<hr />
				<Banner />
				<Switch>
					<Route exact path="/">
						<AlbumsListing />
					</Route>
					<Route path="/add">
						<CreateAlbumForm />
					</Route>
				</Switch>
			</div>
		</Router>
	);
};

// const albums = [
// 	{
// 		title: "Album 1",
// 		artist: "Artist 1",
// 		tracks: ["Track 1", "Track 2", "Track 3"],
// 	},
// 	{
// 		title: "Album 2",
// 		artist: "Artist 2",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 3",
// 		artist: "Artist 3",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// 	{
// 		title: "Album 4",
// 		artist: "Artist 4",
// 		tracks: ["Track 1", "Track 2", "Track 3"],
// 	},
// 	{
// 		title: "Album 5",
// 		artist: "Artist 5",
// 		tracks: ["Track 4", "Track 5", "Track 6"],
// 	},
// 	{
// 		title: "Album 6",
// 		artist: "Artist 6",
// 		tracks: ["Track 6", "Track 7", "Track 9"],
// 	},
// ];

export default App;
