import React from "react";

const AlbumPrice = (props) => props.visible && <p>Price ${props.price}</p>;

export default AlbumPrice;
