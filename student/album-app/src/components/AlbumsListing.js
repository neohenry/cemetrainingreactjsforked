import React, { useState, useEffect } from "react";
import axios from "axios";

import Album from "./Album";

const AlbumsListing = () => {
	const [albums, setAlbums] = useState([]);
	useEffect(() => {
		//console.log("AlbumsListing is mounted");
		axios.get("http://localhost:8081/albums").then((res) => {
			//console.log(res);
			setAlbums(res.data);
		});
		return () => {
			//console.log("AlbumsListing is unmounted");
		};
	}, []);

	return (
		<div className="container">
			<div className="row">
				{albums.map((album) => (
					<Album
						key={album.id}
						title={album.title}
						artist={album.artist}
						tracks={album.tracks}
						price={album.price}
					/>
				))}
			</div>
		</div>
	);
};

export default AlbumsListing;
