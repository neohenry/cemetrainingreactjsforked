import React, { useState } from "react";

import AlbumTitle from "./AlbumTitle";
import AlbumArtist from "./AlbumArtist";
import AlbumTracks from "./AlbumTracks";
import AlbumPrice from "./AlbumPrice";
import ShowHideButton from "./ShowHideButton";

const Album = ({ title, artist, tracks, price }) => {
	const [visible, setVisibity] = useState(true);

	return (
		<div className="col-md-4">
			<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
				<div className="card-body">
					<AlbumTitle title={title} />
					<AlbumArtist artist={artist} />
					<AlbumTracks tracks={tracks} visible={visible} />
					<AlbumPrice price={price} visible={visible} />
					<ShowHideButton toggle={setVisibity} visible={visible} />
				</div>
			</div>
		</div>
	);
};

export default Album;
