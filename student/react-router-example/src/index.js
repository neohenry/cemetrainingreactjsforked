import React from "react";
import ReactDOM from "react-dom";
import App from "./BasicExample";

ReactDOM.render(<App />, document.getElementById("root"));
