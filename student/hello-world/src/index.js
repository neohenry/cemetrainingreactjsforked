import React from "react";
import ReactDOM from "react-dom";

const head1 = React.createElement(
  "p",
  { className: "hello-text", id: "hello-id" },
  "Hello World!"
);

function getFlavours() {
  return (
    <React.Fragment>
      <h2>Ice Cream Flavours</h2>
      <ul custom="ice-cream" className="ice-cream-list">
        <li>Vanilla</li>
        <li>Chocolate</li>
        <li>Raspberry Ripple</li>
        <li>Cookies and Cream</li>
      </ul>
    </React.Fragment>
  );
}

const name = "Ice Cream Shop";
// this is doing using JSX (Javascript XML)
const main = (
  <section>
    <div>
      <h1>{name}</h1>
    </div>
    <div id="ice-cream">{getFlavours()}</div>
  </section>
);

const card = (
  <div className="card" style={{ width: "18rem" }}>
    <div className="card-body">
      <h5 className="card-title">Card title</h5>
      <h6 className="card-subtitle mb-2 text-muted">Card subtitle</h6>
      <p className="card-text">
        Some quick example text to build on the card title and make up the bulk
        of the card's content.
      </p>
      <a href="#" className="card-link">
        Card link
      </a>
      <a href="#" className="card-link">
        Another link
      </a>
    </div>
  </div>
);

ReactDOM.render(card, document.getElementById("root"));

// const PersonFunc = (props) => {
//   const [height, setHeight] = useState(185);

//   return (
//     <div>
//       <p>
//         {props.name} is
//         {height}
//         cm tall.
//       </p>
//       <button onClick={() => setHeight(height + 1)}>Grow</button>
//     </div>
//   );
// };
